import { Component, OnInit } from "@angular/core";

declare var window;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    this.loadScript();
    window.onSignIn = this.userSignIn;
  }

  userSignIn(googleUser) {
    console.log("signed In");
    const profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log("Name: " + profile.getName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail()); // This is null if the 'email' scope is not present.
  }

  public loadScript() {
    const body = document.body as HTMLDivElement;
    const script = document.createElement("script");
    script.innerHTML = "";
    script.src = "https://apis.google.com/js/platform.js";
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
}
